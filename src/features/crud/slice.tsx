import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { brandsAPI } from "api/crud";
import { AppThunk } from "app/store";

import { TBrand, TRoot, TStateTree, TUpdateBrand } from "./types";
import { normalizeView, sortSectionByMain } from "./utils";

const initialState: TStateTree = {
  isLoading: false,
  root: {},
  edittingId: null,
  isCreating: false,
  error: "",
};

export const crudSlice = createSlice({
  name: "crud",
  initialState,
  reducers: {
    setRoot: (state, action: PayloadAction<TRoot>) => {
      state.root = action.payload;
    },
    updateBrand: (state, action: PayloadAction<TUpdateBrand>) => {
      const key = action.payload.rootKey;
      const index = state.root[key].findIndex((v) => v._id === action.payload.item._id);
      state.root[key][index] = action.payload.item;
      state.root[key] = sortSectionByMain(state.root[key]);
    },
    deleteBrand: (state, action: PayloadAction<{ id: string; rootKey: string }>) => {
      const key = action.payload.rootKey;
      state.root[key] = state.root[key].filter((item) => item._id !== action.payload.id);
    },
    addNewBrand(state, { payload }: PayloadAction<TBrand>) {
      const key = payload.title[0].toLocaleUpperCase();
      if (!state.root[key]) state.root[key] = [];
      state.root[key] = [...state.root[key], payload];
      state.root[key] = sortSectionByMain(state.root[key]);
    },
    activateEditMode(state, action: PayloadAction<string | null>) {
      state.edittingId = action.payload;
    },
    setError(state, action: PayloadAction<string>) {
      state.error = action.payload;
    },
    setIsCreation(state, action: PayloadAction<boolean>) {
      state.isCreating = action.payload;
    },
  },
});

export const {
  setRoot,
  updateBrand,
  deleteBrand,
  addNewBrand,
  activateEditMode,
  setError,
  setIsCreation,
} = crudSlice.actions;

export const fetchAllBrands = (): AppThunk => async (dispatch) => {
  const data = await brandsAPI.getAllBrands();
  if (data) {
    dispatch(setRoot(normalizeView(data)));
  }
};

export const updateBrandItemAsync = (data: TUpdateBrand): AppThunk => async (dispatch) => {
  const success = await brandsAPI.updateBrand(data.item._id, data.item);
  dispatch(activateEditMode(null));
  if (success) {
    dispatch(updateBrand(data));
  } else dispatch(setError("updateBrandItemAsync fail"));
};

export const deletBrandItemAsync = (id: string, rootKey: string): AppThunk => async (dispatch) => {
  const success = await brandsAPI.deleteBrand(id);
  dispatch(activateEditMode(null));
  if (success) {
    dispatch(deleteBrand({ id, rootKey }));
  } else dispatch(setError("deletBrandItemAsync fail"));
};

export const addNewBrandAsync = (item: Omit<TBrand, "_id">): AppThunk => async (dispatch) => {
  dispatch(setIsCreation(true));
  const { data, error } = await brandsAPI.addNewBrand(item);
  if (data) {
    dispatch(addNewBrand(data));
  } else dispatch(setError(error));
  dispatch(setIsCreation(false));
};

export const crudReducer = crudSlice.reducer;
