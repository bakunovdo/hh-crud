import { RootState } from "app/store";
import React, { useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";
import { setError } from "../slice";

export const Error = () => {
  const error = useSelector((state: RootState) => state.crud.error);
  const dispatch = useDispatch();

  useEffect(() => {
    setTimeout(() => {
      dispatch(setError(""));
    }, 5000);
  }, [error]);

  return <div style={{ height: "30px", width: "100%", textAlign: "center" }}>{error}</div>;
};
