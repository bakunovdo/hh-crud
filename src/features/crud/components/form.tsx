import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { addNewBrandAsync } from "../slice";

import { RootState } from "app/store";

export const Form = () => {
  const isCreating = useSelector((state: RootState) => state.crud.isCreating);
  const [isMain, setIsMain] = React.useState(false);
  const [inputText, setInputText] = React.useState("");
  const dispatch = useDispatch();

  const createBrandHandler = () => {
    dispatch(addNewBrandAsync({ title: inputText, main: isMain }));
  };

  return (
    <div className="crud-form">
      <input type="text" value={inputText} onChange={(e) => setInputText(e.target.value)} />
      <div>
        <span>main:</span>
        <input type="checkbox" checked={isMain} onChange={(e) => setIsMain(e.target.checked)} />
      </div>
      <button onClick={createBrandHandler} disabled={isCreating}>
        create brand
      </button>
    </div>
  );
};
