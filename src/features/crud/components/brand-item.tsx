import { RootState } from "app/store";
import React, { useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { activateEditMode, deletBrandItemAsync, updateBrandItemAsync } from "../slice";

import { TBrand } from "../types";

type Props = {
  item: TBrand;
  rootKey: string;
};
export const BrandItem: React.FC<Props> = ({ item, rootKey }) => {
  const [isMain, setIsMain] = React.useState(item.main);
  const [inputText, setInputText] = React.useState(item.title);
  const ref = useRef<HTMLDivElement>(null);

  const activeId = useSelector((state: RootState) => state.crud.edittingId);
  const dispatch = useDispatch();

  const editMode = activeId === item._id;
  const defaultClassName = "node__editmode ";
  const editClassName = editMode ? "active" : "";

  const clickHandler = () => {
    setInputText(item.title);
    setIsMain(item.main);
    dispatch(activateEditMode(item._id));
  };

  const saveHandler = () => {
    if (item.title !== inputText.trim() || item.main !== isMain) {
      dispatch(
        updateBrandItemAsync({
          item: { ...item, main: isMain, title: inputText },
          rootKey,
        })
      );
    }
  };

  const deleteHandler = () => {
    dispatch(deletBrandItemAsync(item._id, rootKey));
  };

  const toggleCheckbox = (event: React.ChangeEvent<HTMLInputElement>) => {
    setIsMain(event.target.checked);
  };

  return (
    <div className={defaultClassName + editClassName} ref={ref}>
      {editMode ? (
        <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
          <input type="text" value={inputText} onChange={(e) => setInputText(e.target.value)} />
          <div style={{ display: "flex", alignItems: "center" }}>
            <label>main</label>
            <input type="checkbox" checked={isMain} onChange={toggleCheckbox} />
            <button style={{ marginLeft: "1rem" }} onClick={saveHandler}>
              Save
            </button>
            <button onClick={deleteHandler}>Delete</button>
          </div>
        </div>
      ) : (
        <div onClick={clickHandler}>
          {item.title} {String(item.main)}
        </div>
      )}
    </div>
  );
};
