import React, { useMemo, useState } from "react";

import { TBrand } from "../types";
import { BrandItem } from "./brand-item";

const SHORT_VIEW_ITEMS_COUNTS = 5;

type TProps = {
  items: TBrand[];
  title: string;
};

export const TreeSection = (data: TProps) => {
  const [isExpanded, setIsExpanded] = useState(false);
  const toggleView = (event: React.MouseEvent<HTMLDivElement>) => {
    event.stopPropagation();
    setIsExpanded(!isExpanded);
  };

  const items = useMemo(() => {
    if (isExpanded) return data.items;
    return data.items.slice(0, SHORT_VIEW_ITEMS_COUNTS);
  }, [isExpanded, data.items]);

  if (data.items.length === 0) return null;

  return (
    <div className="node">
      <div className="node_tree-section">
        <div className="node__info" onClick={toggleView}>
          <div style={{ display: "flex" }}>
            <h4 className="node__info__key">{data.title}</h4>
            {data.items.length > SHORT_VIEW_ITEMS_COUNTS && (
              <span className="node__info__status">{isExpanded ? ">>" : "<<"}</span>
            )}
          </div>
          <span className="node__info__counts">{data.items.length}</span>
        </div>
        <div className="node__items">
          {items.map((item) => (
            <BrandItem rootKey={data.title} key={item._id} item={item} />
          ))}
        </div>
      </div>
    </div>
  );
};
