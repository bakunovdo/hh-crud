import { RootState } from "app/store";
import useOnClickOutside from "hooks/useClickOutside";
import React, { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Error, Form, TreeSection } from "./components";
import { activateEditMode, fetchAllBrands } from "./slice";

export const CRUD = () => {
  const root = useSelector((state: RootState) => state.crud.root);
  const dispatch = useDispatch();

  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    dispatch(fetchAllBrands());
  }, []);

  useOnClickOutside(ref, () => {
    dispatch(activateEditMode(null));
  });

  return (
    <div className="crud" ref={ref}>
      <Error />
      <Form />
      {Object.keys(root).map((key) => (
        <TreeSection key={key} items={root[key]} title={key} />
      ))}
    </div>
  );
};
