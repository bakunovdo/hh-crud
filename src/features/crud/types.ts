export type TBrand = {
  _id: string;
  title: string;
  main: boolean;
};

export type TRoot = { [key: string]: TBrand[] };

export type TStateTree = {
  isLoading: boolean;
  root: TRoot;
  edittingId: string | null;
  isCreating: boolean;
  error: string;
};

export type TUpdateBrand = {
  item: TBrand;
  rootKey: string;
};
