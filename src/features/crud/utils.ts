import { TBrand, TRoot } from "./types";

export const groupBrandsByTitle = (data: TBrand[]): TRoot => {
  return data.reduce((acc: TRoot, brand) => {
    const key = brand.title[0].toLocaleUpperCase();
    if (!acc[key]) acc[key] = [];
    acc[key].push(brand);

    return acc;
  }, {});
};

export const sortSectionByMain = (data: TBrand[]) => {
  return data.reduce((acc: TBrand[], brand) => {
    if (brand.main) {
      acc = [brand, ...acc];
    } else {
      acc.push(brand);
    }
    return acc;
  }, []);
};

export const sortRootByMain = (data: TRoot) => {
  const copyData = { ...data };
  Object.keys(copyData).map((key) => {
    copyData[key] = sortSectionByMain(copyData[key]);
  });

  return copyData;
};

export const normalizeView = (data: TBrand[]) => {
  return sortRootByMain(groupBrandsByTitle(data));
};
