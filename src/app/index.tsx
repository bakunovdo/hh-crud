import React from "react";
import { CRUD } from "features/crud";

import "./app.css";

export const App = () => {
  return (
    <div className="app-container">
      <CRUD />
    </div>
  );
};
