import ReactDOM from "react-dom";
import React from "react";

import "./index.css";
import { store } from "./app/store";

import { App } from "./app";

import { Provider } from "react-redux";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
