import axios from "axios";

import { TBrand } from "features/crud/types";

const crudApi = axios.create({
  baseURL: "https://recruting-test-api.herokuapp.com/api/v1",
  headers: {
    "Content-type": "application/json",
  },
});

class BrandsAPI {
  getAllBrands = async () => {
    try {
      const { data } = await crudApi.get<TBrand[]>("/brands");
      return data;
    } catch (error) {
      console.error(error);
    }
  };

  updateBrand = async (id: string, data: TBrand) => {
    try {
      await crudApi.put<TBrand>("/brand/" + id, data);
      return true;
    } catch (error) {
      console.error(error);
    }
  };

  deleteBrand = async (id: string) => {
    try {
      await crudApi.delete<TBrand>("/brand/" + id);
      return true;
    } catch (error) {
      console.error(error);
    }
  };
  addNewBrand = async (item: Omit<TBrand, "_id">) => {
    try {
      const { data } = await crudApi.post<TBrand>("/brands", item);
      return { data, error: "" };
    } catch (error) {
      console.error(error);
      return { data: null, error: error.response.data };
    }
  };
}

export const brandsAPI = new BrandsAPI();
